#!/usr/bin/env bash

# TODO:
# - try for auth: https://kiratechapp.cloud.teamsystem.com:444/api/v1/Docs/it/Home
# - perform login and automate cookies retrieval
# usa questo e prova a usare -b nelle successive chiamate curl
# curl 'https://kiratechapp.cloud.teamsystem.com:444/login.aspx?tsid3=x%2fX9uInzyE8DqpZbw26OzJHPX1nwUMqlAjNwa%2bsp9JV673S0VlhMGtsHwKoF6vEabLv4U3nNwJmrZQbeu%2fYekKpG%2fMrnZ76NTtHSxFWod5jGk2X4HlTl2TJOeS3d7S%2fXlg1xqF0Hq8RxiD39XFDLWy%2bXLU6H%2b%2f9I8S4Up48AkgYouhTBHHbRUCn%2fXmlTr6q5ZV00E%2fnm%2bxKGxDUKfV6tp3iATX%2bDGJqhBy6g1wwuj3LiHfkIAAf893VEoVDHqKnFxNzpwRRdB3QD2lB3i0xRAE4KR9fNWXV6gKr4A23EM7rUo2fgXpmbfdNgwLqEmNFzKWDghySRXDcnkKSGf7TI%2f%2bo2vB7x9MzPyPiK9QuV7uWXlrfLygyR6HOMxbbTG8Sn%2fQxoQHj3rhQ%2fnvg9t%2b6n277mEtToon5a96QO1B3PCq4%3d' \
#   -H 'authority: kiratechapp.cloud.teamsystem.com:444' \
#   -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7' \
#   -H 'accept-language: it-IT,it;q=0.9,en-US;q=0.8,en;q=0.7' \
#   -H 'cache-control: no-cache' \
#   -H 'cookie: CulturePref=it-IT; cookie_choices=%7B%22C0001%22%3Atrue%2C%22C0002%22%3Atrue%2C%22C0005%22%3Afalse%7D; tab_L2NvbW1vbi9wb3BlZGl0YWN0aXZpdHkuYXNweCNTX0FjdGl2aXR5RWRpdF9UYWJiZXJJbnRlcm5hbA$$_Expanded=; tab_L2NvbW1vbi9wb3BwcmludGxpcXVpZC5hc3B4I1R1c3RlbmFUYWJiZXIx_Expanded=; tab_L2FkbWluL25ld3VzZXIuYXNweCNUYWJiZXI$_Expanded=; tab_L2NybS9jb21wYW5pZXMuYXNweCNUYWJiZXI$_Expanded=; tab_L2NvbnRyYWN0L2NvbnRyYWN0LmFzcHgjVGFiYmVy_Expanded=; tab_L2NvbnRyYWN0L2NvbnRyYWN0LmFzcHgjU19BY3Rpdml0eUNocm9ub2xvZ3lfU19FZGl0QWN0aXZpdHlfVGFiYmVySW50ZXJuYWw$_Expanded=; tab_L2NvbW1vbi9wb3BlZGl0am9ib3JkZXJ0YXNrLmFzcHgjU19Kb2JPcmRlclRhc2tFZGl0b3JfU19UYXNrVGFiYmVy_Expanded=; tab_L2NybS9hY3Rpdml0eS5hc3B4I1RhYmJlcg$$_Expanded=; tab_L2NybS9hY3Rpdml0eS5hc3B4I1NfQWN0aXZpdHlFZGl0X1RhYmJlckludGVybmFs_Expanded=; tab_L2NhbGVuZGFyL2FwcG9pbnRtZW50LmFzcHgjVGFiYmVy_Expanded=; PersistLogin=; ASP.NET_SessionId=5c4l0nponlfw0pdkdxbohkkh; __CRMAFToken=a05caabb62a043fd872f15b0c6fae1c0; __CRMAFProc=19448' \
#   -H 'pragma: no-cache' \
#   -H 'referer: https://login.microsoftonline.com/' \
#   -H 'sec-ch-ua: "Not.A/Brand";v="8", "Chromium";v="114", "Google Chrome";v="114"' \
#   -H 'sec-ch-ua-mobile: ?0' \
#   -H 'sec-ch-ua-platform: "macOS"' \
#   -H 'sec-fetch-dest: document' \
#   -H 'sec-fetch-mode: navigate' \
#   -H 'sec-fetch-site: cross-site' \
#   -H 'upgrade-insecure-requests: 1' \
#   -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36' \
#   --compressed

# Note: Need to do login if error occurs.

readonly -a args=('https://kiratechapp.cloud.teamsystem.com:444/AjaxSessionReadHandler.ashx/GetServiceInfo')

# ASP.NET_SessionId is the required cookie containing session data
# readonly -a headers=(
declare -a headers=(
	-H 'authority: kiratechapp.cloud.teamsystem.com:444'
	-H 'accept: /'
	-H 'accept-language: it-IT,it;q=0.9,en-US;q=0.8,en;q=0.7'
	-H 'cache-control: no-cache'
	-H 'content-type: application/x-www-form-urlencoded'
	# -H 'cookie: CulturePref=it-IT; ASP.NET_SessionId=5c4l0nponlfw0pdkdxbohkkh;'
	-H 'origin: https://kiratechapp.cloud.teamsystem.com:444'
	-H 'pragma: no-cache'
	-H 'referer: https://kiratechapp.cloud.teamsystem.com:444/Common/PopEditActivity.aspx?m=25&si=38&linknew=12&isoStart=2024-01-05T07:00:00.000Z&isoEnd=2024-01-05T07:00:00.000Z&allDay=true'
	-H 'sec-ch-ua: "Not.A/Brand";v="8", "Chromium";v="114", "Google Chrome";v="114"'
	-H 'sec-ch-ua-mobile: ?0'
	-H 'sec-ch-ua-platform: "macOS"'
	-H 'sec-fetch-dest: empty'
	-H 'sec-fetch-mode: cors'
	-H 'sec-fetch-site: same-origin'
	-H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36'
)
declare SESSION_FILE='/tmp/tustena.session'
declare SESSION=''
if [[ -n "${1}" ]]; then
	echo "[INFO] Updating session id."
	SESSION="${1}"
	# update session id
	printf "${SESSION}" > "${SESSION_FILE}"
else
	echo "[INFO] Reusing session id."
	# assuming that the session has been previously initialized
	SESSION="$(cat "${SESSION_FILE}")" || exit 1
fi
headers+=(-H "cookie: CulturePref=it-IT; ASP.NET_SessionId=${SESSION};")

readonly -A contract_data_map=(
	["Banca Sella|CAU-522-23"]='Ajax_CallBackType=Digita.Tustena.Common.Service.ContractServiceSelector&Ajax_CallBackMethod=GetServiceInfo&Ajax_CallBackManifest=Tustena&Ajax_CallBackArgument0=834&Ajax_CallBackArgument1=862&Ajax_CallBackArgument2=6'
	["Banca Sella|CAU-528-23"]='Ajax_CallBackType=Digita.Tustena.Common.Service.ContractServiceSelector&Ajax_CallBackMethod=GetServiceInfo&Ajax_CallBackManifest=Tustena&Ajax_CallBackArgument0=850&Ajax_CallBackArgument1=877&Ajax_CallBackArgument2=6'
	["Intesa Sanpaolo|CAU-525-23"]='Ajax_CallBackType=Digita.Tustena.Common.Service.ContractServiceSelector&Ajax_CallBackMethod=GetServiceInfo&Ajax_CallBackManifest=Tustena&Ajax_CallBackArgument0=844&Ajax_CallBackArgument1=871&Ajax_CallBackArgument2=6'
	["Intesa Sanpaolo|CAU-526-23"]='Ajax_CallBackType=Digita.Tustena.Common.Service.ContractServiceSelector&Ajax_CallBackMethod=GetServiceInfo&Ajax_CallBackManifest=Tustena&Ajax_CallBackArgument0=845&Ajax_CallBackArgument1=872&Ajax_CallBackArgument2=6'
	["YNAP|CAU-473-22"]='Ajax_CallBackType=Digita.Tustena.Common.Service.ContractServiceSelector&Ajax_CallBackMethod=GetServiceInfo&Ajax_CallBackManifest=Tustena&Ajax_CallBackArgument0=742&Ajax_CallBackArgument1=773&Ajax_CallBackArgument2=6'
)

readonly -a options=(
	--compressed
	--silent
	--insecure
)

# check credentials
# obtain index
readonly -a contract_data_idx=("${!contract_data_map[@]}")
# probe using the first indexed entry
readonly probe_resp="$(curl "${args[@]}" "${headers[@]}" --data-raw "${contract_data_map["${contract_data_idx[0]}"]}" "${options[@]}")"
if [[ -n "$(echo "${probe_resp}" | jq -r '.error // empty' -)" ]]; then
	echo "[ERROR] Login required."
	exit 1
fi

declare header='Client|Contract|Remaining Hours\n---|---|---\n'
declare table=''
for contract in "${!contract_data_map[@]}"; do
	# echo "contract: $contract"
	# echo "value: ${contract_data_map[${contract}]}"
	# JSON response
	declare resp="$(curl "${args[@]}" "${headers[@]}" --data-raw "${contract_data_map[${contract}]}" "${options[@]}")"
	# hours in format hh.mm
	declare hours="$(awk -F'rimangono|ore' '{print $2}' <<< "$(echo ${resp} | jq -r '.value.consumptionconfig')" | tr -d ' ' | sed 's/:/./g')"
	# concatenate new entry
	table+="${contract}|${hours}\n"
done

# print current data
printf "[INFO] Date: $(date +'%d/%m/%Y - %H:%M').\n"
# sort table
readonly sorted_table="$(printf "${table}" | sort -s -k 2 | sort -s -k 1)\n"
# print in tabular format
printf "${header}${sorted_table}" | column -x -t -s "|"